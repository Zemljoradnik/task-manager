import React from 'react';
import {
  Components,
  replaceComponent,
  withCurrentUser,
} from 'meteor/vulcan:core';

const Layout = ({ currentUser }) => (
  <div className="layout">
    {!currentUser && <Components.Login/>}

    {currentUser &&
      <div className="private">
        <div className="header">
          <div className="app-title">Task manager</div>
          <div className="header-right-group">
            <div className="user-name">{currentUser.displayName}</div>
            <Components.AccountsLoginForm /> 
          </div>
        </div>

        <div className="content">
          <Components.TasksList/>
        </div>
      </div>
    }
  </div>
);

replaceComponent('Layout', withCurrentUser(Layout));
