import React from 'react';
import {
  Components,
  registerComponent,
  withCurrentUser,
} from 'meteor/vulcan:core';

const Login = ({ currentUser = {}}) => (
  <div className="login">
    <h2 className="title">
      Task manager
    </h2>

    {currentUser &&
      <p className="user-name">
        {currentUser.displayName}
      </p>
    }

    <Components.AccountsLoginForm className="login-form"/> 
  </div>
);

registerComponent({
  name: 'Login',
  component: Login,
  hocs: [withCurrentUser],
});
