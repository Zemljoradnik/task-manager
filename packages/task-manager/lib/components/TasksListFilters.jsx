import React, { Component } from 'react';
import {
  Components,
  registerComponent,
} from 'meteor/vulcan:core';

const TasksListFilters = ({ data = {}, handleFilterChange }) => {
  const users = data.users || {};
  const results = users.results;

  if (!Array.isArray(results)) {
    return false;
  }

  return (
    <div className="task-list-filters">
      <div className="task-list-filter">
        <label className="task-list-filter-label">
          Filter by task text
        </label>
        <Components.FormComponentDefault
          inputProperties={{
            onChange: handleFilterChange.bind(null, 'textFilter'),
            placeholder: 'Type to filter',
        }}/>
      </div>

      <div className="task-list-filter">
        <label className="task-list-filter-label">
          Filter by assignee
        </label>
        <Components.FormComponentSelect
          inputProperties={{
            options: [{
                value: '',
                label: 'Show all',
            }].concat(results.map(user => ({
                value: user._id,
                label: user.displayName,
            }))),
            onChange: handleFilterChange.bind(null, 'assigneeIdFilter'),
        }}/>
      </div>

      <div className="task-list-filter">
        <label className="task-list-filter-label">
          Filter by creator
        </label>
        <Components.FormComponentSelect
          inputProperties={{
            options: [{
                value: '',
                label: 'Show all',
            }].concat(results.map(user => ({
                value: user._id,
                label: user.displayName,
            }))),
            onChange: handleFilterChange.bind(null, 'creatorIdFilter'),
        }}/>
      </div>
    </div>
  )
}

registerComponent({
  name: 'TasksListFilters',
  component: TasksListFilters,
});

export default TasksListFilters
