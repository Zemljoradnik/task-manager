import React, { Component } from 'react';
import {
  Components,
  registerComponent,
  withDelete,
} from 'meteor/vulcan:core';

import Tasks from '../modules/collection.js';

class DeleteTask extends Component {
  constructor() {
    super(...arguments);

    this.handleDeleteTask = this.handleDeleteTask.bind(this);
  }

  handleDeleteTask(event) {
    const {
      document = {},
      deleteTask,
    } = this.props;

    if (window.confirm("Are you sure you want to delete this task?")) {
      deleteTask({
        selector: {
          documentId: document._id,
        },
      })
    }
  }

  render() {
    return (
      <a
        href="javascript:void(0)"
        className="delete-button"
        onClick={this.handleDeleteTask}
      >
        Delete
      </a>
    )
  }
}

const options = {
  collection: Tasks,
  fragmentName: 'TasksFragment',
};

registerComponent({
  name: 'DeleteTask',
  component: DeleteTask,
  hocs: [
    [withDelete, options],
  ],
});
