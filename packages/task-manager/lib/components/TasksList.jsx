import React, { Component } from 'react';
import {
  Components,
  registerComponent,
  withMulti,
} from 'meteor/vulcan:core';
import Users from 'meteor/vulcan:users';

import Tasks from '../modules/collection.js';

const columns = data => [
  {
    label: 'Task',
    sortable: 'text',
    component: ({ document }) =>
      <>
        <span className="cell-header">Task</span>
        <span className="task-text">{document.text}</span>
      </>,
  },
  {
    label: 'Assignee',
    sortable: 'assignee',
    component: ({ document }) =>
      <>
        <span className="cell-header">Assignee</span>
        <Components.Assignee
          document={document}
          data={data}
        />
      </>,
  },
  {
    label: 'Due date',
    sortable: 'dueDate',
    component: ({ document = {}}) =>
      <>
        <span className="cell-header">Due date</span>
        <span>{new Date(document.dueDate).toDateString()}</span>
      </>,
  },
  {
    label: 'Creator',
    sortable: 'creator',
    component: ({ document = {} }) => {
      const creator = document.creator || {};

      return (
        <>
          <span className="cell-header">Creator</span>
          <span>{creator.displayName}</span>
        </>
      );
    },
  },
  {
    label: 'Creation date',
    sortable: 'createdAt',
    component: ({ document = {}}) =>
      <>
        <span className="cell-header">Creation date</span>
        <span>{new Date(document.createdAt).toDateString()}</span>
      </>,
  },
  {
    label: 'Delete',
    component: ({ document }) =>
      <Components.DeleteTask document={document}/>
  },
];

const tasksOptions = (filters = {}) => ({
  collection: Tasks,
  fragmentName: 'TasksFragment',
  pollInterval: 5000,
  terms: {
    view: 'filter',
    assigneeId: filters.assigneeIdFilter,
    userId: filters.creatorIdFilter,
    text: {$regex: `.*${filters.textFilter}.*`},
  },
});

const usersOptions = {
  collection: Users,
  fragmentName: 'UsersFragment',
};

class TasksList extends Component {
  constructor() {
    super(...arguments);

    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.state = {
      assigneeIdFilter: '',
      creatorIdFilter: '',
      textFilter: '',
    };
  }

  handleFilterChange(filterType, event) {
    if (filterType === 'showPastDueFilter') {
      this.setState({
        [filterType]: event.currentTarget.checked,
      });

    } else {
      this.setState({
        [filterType]: event.currentTarget.value,
      });
    }
  }

  render() {
    const {
      data = {},
    } = this.props;

    const users = data.users;

    return (
      <div className="tasks-list">
        <Components.TasksListFilters
          data={data}
          handleFilterChange={this.handleFilterChange}
        />

        <Components.Datatable 
          collection={Tasks} 
          options={tasksOptions(this.state)}
          columns={columns(data)}
          newFormOptions={{
            users,
            label: 'New task',
          }}
          editFormOptions={{
            users,
          }}
          showSearch={false}
        />
      </div>
    );
  }
}

registerComponent({
  name: 'TasksList',
  component: TasksList,
  hocs: [
    [withMulti, usersOptions],
  ],
});
