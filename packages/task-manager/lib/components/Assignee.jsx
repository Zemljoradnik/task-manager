import React, { Component } from 'react';
import {
  Components,
  registerComponent,
  withUpdate,
} from 'meteor/vulcan:core';

import Tasks from '../modules/collection.js';

class Assignee extends Component {
  constructor() {
    super(...arguments);

    this.handleChangeAssignee = this.handleChangeAssignee.bind(this);
  }

  handleChangeAssignee(event) {
    const {
      document,
      updateTask,
    } = this.props;

    updateTask({
      selector: {
        documentId: document._id,
      },
      data: {
        assigneeId: event.currentTarget.value,
      },
    })
  }

  render() {
    const {
      document = {},
      data = {},
    } = this.props;

    const assignee = document.assignee;
    const users = data.users || {};
    const results = users.results;

    if (!Array.isArray(results)) {
      return false;
    }

    return (
      <Components.FormComponentSelect
        inputProperties={{
          options: results.map(user => ({
              value: user._id,
              label: user.displayName,
          })),
          value: assignee._id,
          onChange: this.handleChangeAssignee,
      }}/>
    )
  }
}

const options = {
  collection: Tasks,
  fragmentName: 'TasksFragment',
};

registerComponent({
  name: 'Assignee',
  component: Assignee,
  hocs: [
    [withUpdate, options],
  ],
});
