const schema = {

  _id: {
    type: String,
    optional: true,
    canRead: ['guests'],
  },

  createdAt: {
    type: Date,
    optional: true,
    canRead: ['members'],
    onCreate: () => {
      return new Date();
    },
  },

  userId: {
    type: String,
    optional: true,
    canRead: ['members'],
    resolveAs: {
      fieldName: 'creator',
      type: 'User',
      resolver: async (task, args, context) => {
        return await context.Users.loader.load(task.userId);
      },
      addOriginalField: true,
    },
  },

  assigneeId: {
    type: String,
    input: 'select',
    canRead: ['members'],
    canCreate: ['members'],
    canUpdate: ['members'],
    resolveAs: {
      fieldName: 'assignee',
      type: 'User',
      resolver: async (task, args, context) => {
        return await context.Users.loader.load(task.assigneeId);
      },
      addOriginalField: true,
    },
    options: props => props.users.results.map(user => ({
      value: user._id,
      label: user.displayName,
    })),
  },

  dueDate: {
    type: Date,
    optional: true,
    canRead: ['members'],
    canCreate: ['members'],
    canUpdate: ['members'],
  },

  text: {
    label: 'Task',
    type: String,
    input: 'textarea',
    canRead: ['members'],
    canCreate: ['members'],
    canUpdate: ['members'],
    searchable: true,
  },

};

export default schema;
