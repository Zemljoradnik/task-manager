import Tasks from './collection.js';

Tasks.addView('filter', terms => {
  const selector = {};

  if (terms.assigneeId !== '') {
    selector.assigneeId = terms.assigneeId;
  }

  if (terms.userId !== '') {
    selector.userId = terms.userId;
  }

  if (terms.text !== '') {
    selector.text = terms.text;
  }

  return {
    selector,
  }
});

