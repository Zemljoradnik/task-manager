import Users from 'meteor/vulcan:users';

const membersActions = [
  'task.create',
  'task.update.all',
  'task.delete.all',
  'task.update.own',
  'task.delete.own',
];

Users.groups.members.can(membersActions);
