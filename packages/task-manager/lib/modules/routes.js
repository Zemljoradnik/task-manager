import { addRoute } from 'meteor/vulcan:core';

addRoute({ name: 'Layout', path: '/', componentName: 'Layout' });
