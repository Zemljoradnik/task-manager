import {
  createCollection,
  getDefaultResolvers,
  getDefaultMutations,
} from 'meteor/vulcan:core';
import { Connectors } from 'meteor/vulcan:lib';

import schema from './schema.js';

const tasksResolvers = getDefaultResolvers('Tasks');

// Overriding 'multi' resolver so we can sort by creator or assignee displayName;
// copied and amended code from vulcan:core/default_resolvers.js
tasksResolvers.multi = {
  description: 'Overriden multi resolver',
  async resolver(root, { input = {} }, context, { cacheControl }) {
    const { terms = {}, enableCache = false, enableTotal = true } = input;

    if (cacheControl && enableCache) {
      const maxAge = resolverOptions.cacheMaxAge || defaultOptions.cacheMaxAge;
      cacheControl.setCacheHint({ maxAge });
    }

    const { currentUser, Users } = context;
    const collection = context.Tasks;

    let { selector, options } = await collection.getParameters(terms, {}, context);
    options.skip = terms.offset;

    const docs = options.sort.creator || options.sort.assignee
      ? await getSortedTasks(collection, selector, options, Users).toArray()
      : await Connectors.find(collection, selector, options);

    const viewableDocs = collection.checkAccess
      ? _.filter(docs, doc => collection.checkAccess(currentUser, doc))
      : docs;

    const restrictedDocs = Users.restrictViewableFields(currentUser, collection, viewableDocs);

    restrictedDocs.forEach(doc => collection.loader.prime(doc._id, doc));

    const data = { results: restrictedDocs };

    if (enableTotal) {
      data.totalCount = await Connectors.count(collection, selector);
    }

    return data;
  }
};

// Sort by creator or assignee displayName
const getSortedTasks = (collection, selector, options) => {
  const userIdAttribute = options.sort.creator
    ? 'userId'
    : 'assigneeId';
  const sortingOrder = options.sort.creator || options.sort.assignee;
  delete options.sort.creator;
  delete options.sort.assignee;

  // this expects the collection to be a Mongo collection;
  // if database connector gets changed this might fail,
  // but it was used as it's faster then manual sort on FE
  return collection.aggregate([
    {
      '$lookup': {
        from: 'users',
        localField: userIdAttribute,
        foreignField: '_id',
        as: 'user'
      }
    },
    {
      '$sort': {
        'user.displayName': sortingOrder,
        ...options.sort
      }
    },
    {
      '$limit': options.limit
    }
  ]);
};

export default createCollection({
  collectionName: 'Tasks',
  typeName: 'Task',
  schema,
  resolvers: tasksResolvers,
  mutations: getDefaultMutations('Tasks'),
});
