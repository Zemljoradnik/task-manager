import { registerFragment } from 'meteor/vulcan:core';

registerFragment(/* GraphQL */`
  fragment TasksFragment on Task {
    _id
    createdAt
    dueDate
    text
    creator {
      _id
      displayName
    }
    assignee {
      _id
      displayName
    }
  }
`);

registerFragment(/* GraphQL */`
  fragment UsersFragment on User {
    _id
    displayName
  }
`);
